import './App.css';
import { useState } from 'react';

import NewExpense from './components/NewExpense/NewExpense'
import Expenses from './components/Expenses/Expenses'

const initialCoinHistory = [
  {
    buyDate: new Date(2021, 8, 28),
    buyAmount: 1,
    eurAmount: 300
  },
  {
    buyDate: new Date(2021, 2, 28),
    buyAmount: 6,
    eurAmount: 80
  },
  {
    buyDate: new Date(2021, 1, 28),
    buyAmount: 2,
    eurAmount: 550
  },
  {
    buyDate: new Date(2021, 1, 28),
    buyAmount: 2,
    eurAmount: 550
  },
]

function App() {
  const [expenses, setExpenses] = useState(initialCoinHistory);

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [{ buyAmount: expense.btc, eurAmount: expense.eur, buyDate: expense.date }, ...prevExpenses];
    });
  }


  return (
    <div className="App">
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses coinHistory={expenses} />
    </div>
  );
}

export default App;