import './NewExpense.css';
import ExpenseForm from './ExpenseForm';
import { useState } from 'react';

const NewExpense = (props) => {
    const saveExpeseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString
        };
        props.onAddExpense(expenseData)
    }

    const [isShowing, setIsShowing] = useState(false);

    return (
        <div className="new-expense">
            {isShowing
                ? (<ExpenseForm setIsShowingFunc={setIsShowing} onSaveExpenseData={saveExpeseDataHandler} />)
                : (
                    <div className="">
                        <button onClick={() => setIsShowing(!isShowing)}>Add transaction</button>
                    </div>
                )
            }
        </div>
    )
}

export default NewExpense;