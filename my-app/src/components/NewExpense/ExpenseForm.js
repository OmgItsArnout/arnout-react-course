import { useState } from 'react';

import './ExpenseForm.css';

const ExpenseForm = (props) => {
    const [enteredAmountBtc, setEnteredAmountBtc] = useState('');
    const [enteredAmountEuro, setEnteredAmountEuro] = useState('');
    const [enteredDate, setEnteredDate] = useState('');

    // const [userInput, setUserInput] = useState({
    //     enteredAmountBtc: 0,
    //     enteredAmountEuro: 0,
    //     enteredDate: ''
    // })

    const eurAmountChangeHandler = (event) => {
        setEnteredAmountEuro(event.target.value);
        // setUserInput({
        //     ...userInput,
        //     enteredAmountEuro: event.action.value,
        // })
        // setUserInput((prevState) => {
        //     return {
        //         ...prevState,
        //         enteredAmountEuro: event.action.value,
        //     }
        // })
    }

    const btcAmountChangeHandler = (event) => {
        setEnteredAmountBtc(event.target.value);
        // setUserInput({
        //     ...userInput,
        //     setEnteredAmountBtc: event.action.value,
        // })
    }

    const dateChangeHandler = (event) => {
        setEnteredDate(event.target.value);
        // setUserInput({
        //     ...userInput,
        //     enteredDate: event.action.value,
        // })
    }

    const submitHandler = (event) => {
        event.preventDefault();
        const expenseData = {
            btc: enteredAmountBtc,
            eur: enteredAmountEuro,
            date: new Date(enteredDate)
        }
        setEnteredDate('');
        setEnteredAmountEuro('');
        setEnteredAmountBtc('');
        props.setIsShowingFunc(false);
        props.onSaveExpenseData(expenseData);
    };

    return (
        <form onSubmit={submitHandler}>
            <div className="new-expense__controls">
                <div className="new-expense__control">
                    <label>Amount of BTC (negative for sell)</label>
                    <input value={enteredAmountBtc} type="number" step='0.01' onChange={btcAmountChangeHandler} />
                </div>
                <div className="new-expense__control">
                    <label>Amount in Euro</label>
                    <input value={enteredAmountEuro} type="number" onChange={eurAmountChangeHandler} />
                </div>
                <div className="new-expense__control">
                    <label>Date</label>
                    <input value={enteredDate} type="date" min='2019-01-01' max='2022-12-31' onChange={dateChangeHandler} />
                </div>
            </div>
            <div className="new-expense__actions">
                <button onClick={() => props.setIsShowingFunc(false)}>cancel</button>
                <button type='submit'>Add transaction</button>
            </div>
        </form>
    )
}

export default ExpenseForm