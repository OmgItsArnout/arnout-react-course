import ExpenseItem from "./ExpenseItem";
import './ExpensesList.css';

const ExpensesList = props => {
    let expensesContent = <p>No expenses found.</p>;

    if (props.items.length === 0) {
        return <h2 className="expenses-list__fallback">Found no transactions.</h2>
    }

    return <ul className="expenses-list">
        {props.items.map(coinItem => (
        <ExpenseItem 
          key={coinItem.id}
          buyDate={coinItem.buyDate} 
          buyAmount={coinItem.buyAmount}
          eurAmount={coinItem.eurAmount} />
      ))}
    </ul>
}

export default ExpensesList;