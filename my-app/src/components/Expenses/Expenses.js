import Card from '../UI/Card';
import ExpenseItem from './ExpenseItem';
import './Expenses.css';
import { useState } from 'react';
import ExpenseFilter from './ExpenseFilter'
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';

function Expenses(props) {
  const [filterYear, setFilterYear] = useState('2020');

  const handleChangeFilter = (year) => {
    setFilterYear(year);
  }

  function checkYear(coinItem) {
    if (coinItem.buyDate.getFullYear().toString() === filterYear) {
      return coinItem;
    }
  }

  const filteredExpenses = props.coinHistory.filter(checkYear)

  return (
    <Card className="expenses">
      <ExpenseFilter onChangeFilter={handleChangeFilter} />
      <ExpensesChart expenses={filteredExpenses} />
      <ExpensesList items={filteredExpenses} />
    </Card>
  )
}

export default Expenses;