import React, { useState } from 'react';

import ExpenseDate from './ExpenseDate';
import Card from '../UI/Card';
import './ExpenseItem.css'

function ExpenseItem(props) {

    let [title, setTitle] = useState('beforeTitle');

    const clickHandler = () => {
        setTitle('updated!');
    }
    return (
        <li>
            <Card className='expense-item'>
                <ExpenseDate date={props.buyDate} />
                <div className="expense-item__description">
                    {(props.buyAmount > 0) ? (
                        <h2>Added Bitcoin to portfolio</h2>
                    ) : (
                        <h2>Sold Bitcoin from portfolio</h2>
                    )}
                    <span>€{props.eurAmount}</span>
                    <div className="expense-item__price">{props.buyAmount} BTC</div>
                    <button onClick={clickHandler}>change title</button>
                </div>
            </Card>
        </li>
    );
}

export default ExpenseItem;