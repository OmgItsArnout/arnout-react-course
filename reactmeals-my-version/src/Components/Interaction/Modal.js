import { useContext } from "react";
import FoodContext from "../Context/FoodContext";
import classes from "./Modal.module.css";
import BasketItem from "./BasketItem"
import Button from "./Button";

const Modal = () => {
    const context = useContext(FoodContext);

    if (context.showBasket) {
        return (
            <div className={classes.modalBackground}>
                <div className={classes.modalContainer}>
                    {context.orderedFoods.map((foodItem) => {
                        return (
                            <BasketItem
                                amount={foodItem.amount}
                                name={foodItem.name}
                                price={foodItem.price}
                                id={foodItem.id}
                            />
                        );
                    })}
                    <div className={classes.buttonContainer}>
                        <Button onClick={() => context.setShowBasket(false)}>Close</Button>
                        <Button>Order</Button>
                    </div>

                </div>
            </div>
        );
    }

    return "";

}

export default Modal;