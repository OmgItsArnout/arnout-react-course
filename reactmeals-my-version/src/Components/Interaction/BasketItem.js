import classes from './BasketItem.module.css';
import { useContext } from 'react';
import FoodContext from '../Context/FoodContext';

const BasketItem = (props) => {
    const ctx = useContext(FoodContext);

    return (
        <div className={classes.BasketItemContainer}>
            <div className={classes.leftColumn}>
                <span className={classes.name}>{props.name}</span>
                <span className={classes.price}>${props.price}</span>
                <span className={classes.times}>x{props.amount}</span>
            </div>
            <div className={classes.rightColumn}>
                <button onClick={() => ctx.onAddToBasket(props.id, -1)}>-</button>
                <button onClick={() => ctx.onAddToBasket(props.id, 1)}>+</button>
            </div>
        </div>
    )
}

export default BasketItem;