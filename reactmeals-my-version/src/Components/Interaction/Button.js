import classes from './Button.module.css'

const Button = (props) => {
    return (
        <div onClick={props.onClick} onKeyDown={props.onClick} className={`${classes.button} ${classes[props.color]}`}>
            {props.children}
        </div>
    );
}

export default Button