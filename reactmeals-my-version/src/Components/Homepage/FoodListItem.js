import { useContext, useRef } from 'react'
import FoodContext from '../Context/FoodContext'
import Button from '../Interaction/Button'
import classes from './FoodListItem.module.css'

const FoodListItem = (props) => {
    const ctx = useContext(FoodContext);
    const inputRef = useRef(null);

    return (
        <div className={classes.foodListItem}>
            <div className={classes.leftColumn}>
                <span className={classes.name}>{props.name}</span>
                <span className={classes.description}>{props.description}</span>
                <span className={classes.price}>${props.price}</span>
            </div>
            <div className={classes.rightColumn}>
                <div className={classes.amountContainer}>
                    <span className={classes.amount}>Amount</span>
                    <input ref={inputRef} type="number" defaultValue="1" />
                </div>
                <Button onClick={() => ctx.onAddToBasket(props.id, parseInt(inputRef.current.value))}>+Add</Button>
            </div>
        </div>
    )
}

export default FoodListItem;