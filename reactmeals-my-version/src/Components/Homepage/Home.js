import classes from './Home.module.css';
import FoodList from './FoodList'

const Home = () => {
    return (
        <div className={classes.homeContainer}>
            <div className={classes.img} />
            <div className={classes.introText}>
                <h1>Delicious Food, Delivered To You</h1>
                <p>Choose your favorite meal from our broad selection of available meals and enjoy a delicious lunch or dinner at home. All our meals are cooked with high-quality ingredients, just-in-time and of course by experienced chefs!</p>
            </div>
            <div className={classes.foodListContainer}>
                <FoodList />
            </div>

        </div>
    );
}

export default Home;