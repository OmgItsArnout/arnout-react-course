import { useContext } from 'react';
import FoodContext from '../Context/FoodContext';
import classes from './FoodList.module.css';
import FoodListItem from './FoodListItem';

const FoodList = () => {
    const ctx = useContext(FoodContext)
    return (
        <div className={classes.FoodListContainer}>
            {ctx.foods.map((food) => {
                return (
                    <FoodListItem
                        name={food.name}
                        description={food.description}
                        price={food.price}
                        id={food.id}
                    />
                );
            })}
        </div>
    );
}

export default FoodList;