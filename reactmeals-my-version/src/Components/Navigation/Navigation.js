import { useContext } from 'react';
import FoodContext from '../Context/FoodContext';
import Button from '../Interaction/Button';
import classes from './Navigation.module.css'
import Modal from '../Interaction/Modal';

const Navigation = () => {
    const ctx = useContext(FoodContext);

    return (
        <>
            <div className={classes.NavigationContainer}>
                <span>ReactMeals</span>
                <Button onClick={() => ctx.setShowBasket(!ctx.showBasket)}>Your Cart</Button>
            </div>
            <Modal />
        </>
    );
}

export default Navigation;