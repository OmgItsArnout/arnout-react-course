import React, { useState, useEffect } from 'react';

const FoodContext = React.createContext({
    foods: [],
    orderedFoods: [],
    onAddToBasket: (id => { }),
    showBasket: false,
    setShowBasket: (() => { })
});

export const FoodContextProvider = (props) => {
    const [basket, setBasket] = useState([]);
    const [showBasket, setShowBasket] = useState(false);

    const foods = [
        {
            id: 1,
            name: 'Sushi',
            description: 'Finest fish and veggies',
            price: '22.99'
        },
        {
            id: 2,
            name: 'Schnitzel',
            description: 'A german speciality!',
            price: '15.99'
        }];

    const AddToBasketHandler = (id, amount) => {
        let isAdded = false;

        basket.forEach((food, i) => {
            if (id === food.id) {
                let newBasket = basket.slice();
                food.amount += amount;
                if (food.amount === 0) {
                    setBasket(newBasket.splice(i, 1));
                    isAdded = true;
                    return;
                } else {
                    newBasket[i] = food;
                    setBasket(newBasket);
                    isAdded = true;
                    return;
                }
            }
        })

        if (isAdded) return;

        foods.forEach((food) => {
            if (id === food.id) {
                food.amount = amount;
                setBasket([...basket, food]);
                return;
            }
        })

    }

    return <FoodContext.Provider value={{
        foods: foods,
        orderedFoods: basket,
        onAddToBasket: AddToBasketHandler,
        showBasket: showBasket,
        setShowBasket: setShowBasket,
    }}>
        {props.children}
    </FoodContext.Provider>
}

export default FoodContext;