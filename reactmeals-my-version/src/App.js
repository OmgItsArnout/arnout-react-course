import './App.css';
import Navigation from './Components/Navigation/Navigation';
import Home from './Components/Homepage/Home';
import { useContext } from 'react';
import { FoodContextProvider } from './Components/Context/FoodContext';
import Modal from './Components/Interaction/Modal';

function App() {
  return (
    <FoodContextProvider>
      <div className="App">
        <Navigation />
        <Home />
      </div>
    </FoodContextProvider>
  );
}

export default App;
