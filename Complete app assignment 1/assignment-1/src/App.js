import classes from './App.module.css';
import AddUser from './Components/AddUser';
import { useState } from 'react'
import UserList from './Components/UserList';
import Modal from './Components/Modal';

function App() {
  const [users, setUsers] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const addUser = (user) => {
    if (user.name === '' || user.age === '') {
      setShowModal(true);
      return;
    }
    setUsers([...users, user]);
  }

  const removeUser = userId => {
    setUsers(users.filter(user => user.id !== userId))
  }

  return (
    <>
      <div className={classes.app}>
        <AddUser addUserFunc={addUser} />
        {users.length && (
          <UserList users={users} removeUserFunc={removeUser} />
        )}

      </div>
      {showModal && (
        <Modal setShowModalFunc={setShowModal} />
      )}
    </>
  );
}

export default App;