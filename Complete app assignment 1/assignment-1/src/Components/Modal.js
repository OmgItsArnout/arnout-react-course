import classes from './Modal.module.css';

const Modal = (props) => {
    return (
        <div className={classes.modalContainer}>
            <div className={classes.modal}>
                <h2>Please insert both username and age.</h2>
                <button onClick={() => props.setShowModalFunc(false)}>go back</button>
            </div>
        </div>
    )
}

export default Modal;