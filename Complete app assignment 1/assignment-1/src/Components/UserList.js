import classes from './UserList.module.css';

const UserList = (props) => {
    return (
        <div className={classes.container}>
            {props.users.map((user) => {
                return (
                    <span onClick={() => props.removeUserFunc(user.id)} className={classes.userItem} key={user.id}>{user.name + '(' + user.age + ')'}</span>
                )
            })}
        </div>
    )
}

export default UserList;