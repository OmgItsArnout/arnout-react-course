import classes from './AddUser.module.css';

const AddUser = (props) => {

    const handleSubmit = (event) => {
        event.preventDefault();
        props.addUserFunc({
            name: event.target.elements.name.value,
            age: event.target.elements.age.value,
            id: Math.random()
        });
    }

    return (
        <>
            <div className={classes.container}>
                <form onSubmit={handleSubmit}>
                    <span>Username</span>
                    <input type="text" name="name" />
                    <span>Age</span>
                    <input type="number" name="age" />
                    <input type="submit" value="Add User" />
                </form>
            </div>
        </>
    )
}

export default AddUser;