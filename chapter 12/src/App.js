import React, { useState, useCallback } from 'react';

import './App.css';
import DemoOutput from './components/demo/DemoOutput';
import Button from './components/UI/Button/Button'

function App() {
  const [showParagraph, setShowParagraph] = useState(false);
  const [allowToggle, setAllowToggle] = useState(false);

  console.log('app running');

  const toggleParagraphHandler = useCallback(() => {
    if (allowToggle) {
      setShowParagraph(prevShowParagraph => !prevShowParagraph);
    }
  }, [allowToggle]);

  const allowToggleHandler = () => {
    setAllowToggle(true);
  }

  return (
    <div className="app">
      <h1>Hi there!</h1>
      <Button onClick={toggleParagraphHandler}>show paragraph</Button>
      <Button onClick={allowToggleHandler}>allow toggling</Button>
      <DemoOutput show={showParagraph} />
    </div>
  );
}

export default App;
