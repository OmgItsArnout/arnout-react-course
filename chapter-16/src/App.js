import SimpleInput from './components/SimpleInput';

function App() {
  return (
    <div className="app">
      <BasicInput />
    </div>
  );
}

export default App;
